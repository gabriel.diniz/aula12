import java.util.Scanner;

public class aula12 {  //média de notas

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        double nota1, nota2, nota3, nota4;  // 4 variaveis , 0.0

        System.out.println("Digite a sua nota: "); // imprime a mensagem
        nota1 = in.nextDouble(); // guardando valor da nota na variavel nota 1

        System.out.println("Digite a sua 2 nota: "); // imprime a mensagem
        nota2 = in.nextDouble(); // guardando valor da nota na variavel nota 2

        System.out.println("Digite a sua 3 nota: "); // imprime a mensagem
        nota3 = in.nextDouble(); // guardando valor da nota na variavel nota 3

        System.out.println("Digite a sua 4 nota: "); // imprime a mensagem
        nota4 = in.nextDouble(); // guardando valor da nota na variavel nota 4

        double soma = nota1+nota2+nota3+nota4;  //soma de notas
        soma = soma / 4; //soma dividido pela quantidade de notas , 4

        //System.out.println( soma );  // imprime o valor da soma

        if(soma >= 7) {  //maior ou igual a 7, foi aprovado
            System.out.println("Você foi aprovado, sua média foi de: " + soma);  // imprime a mensagem
        }else{
            System.out.println("Você foi reprovado, sua média foi de: " + soma);  // imprime a mensagem

        }

    }
}

